# world-karts-champion

## Development server

Run `ng serve` or `npm run start` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` or `npm run test` to execute the unit tests.

## How to access to different modes (tv mode, static mode)

- To get the app on a static mode (user interact with app), navigate to `http://localhost:4200/`

- To get dynamic tv mode navigate to `http://localhost:4200/dashboard?secs=20`
  `secs` parameter it's the time (in secs) used to show each different tab (general ranking/driver info/race info)
  if `secs` it's not provided a default time of 10 seconds per tab, will be used
