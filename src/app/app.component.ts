import { TranslateProviderService } from './services/translate-provider-service/translate-provider.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent implements OnInit {
  constructor(private readonly translationProvider: TranslateProviderService) {}
  ngOnInit(): void {
    this.translationProvider.initialize();
  }
}
