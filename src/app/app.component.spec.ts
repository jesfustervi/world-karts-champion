import { TranslateProviderService } from './services/translate-provider-service/translate-provider.service';
import { TestBed, async } from '@angular/core/testing';
import { AppComponent } from './app.component';
import { TranslateTestingModule } from './shared/tests/modules';

let component: AppComponent;
let translationProvider: TranslateProviderService;
describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [AppComponent],
      imports: [TranslateTestingModule],
    }).compileComponents();
    const fixture = TestBed.createComponent(AppComponent);
    component = fixture.componentInstance;
    translationProvider = TestBed.inject(TranslateProviderService);
  }));

  it('should create the app', () => {
    expect(component).toBeTruthy();
  });

  it('should initialize translate provider', () => {
    spyOn(translationProvider, 'initialize');
    component.ngOnInit();
    expect(translationProvider.initialize).toHaveBeenCalled();
  });
});
