import { TranslateProviderService } from './../../services/translate-provider-service/translate-provider.service';
import { DriverRaceDetails } from './../../models/driver-race-details';
import { ActivatedRoute, Router } from '@angular/router';
import { Race } from './../../models/race';
import { Driver } from './../../models/driver';
import { of } from 'rxjs';

export const RaceMockFast: Race = {
  name: 'RaceTest',
  time: '19:18:46.34',
  timeMilliSecs: 2313,
  position: 1,
  points: 25,
};

export const RaceMockTwoSlow: Race = {
  name: 'RaceTestTwo',
  time: '28:18:46.34',
  timeMilliSecs: 424141,
  position: 4,
  points: 17,
};

export const RaceMockThreeMedium: Race = {
  name: 'RaceTestTwo',
  time: '12:18:46.34',
  timeMilliSecs: 424141,
  position: 4,
  points: 17,
};

export const DriverMockFaster: Driver = {
  _id: 'idTest',
  picture: 'pictureTest',
  age: 35,
  name: 'DriverOne',
  team: 'TeamTest',
  races: new Array(RaceMockFast),
  position: 1,
  points: 25,
  bestTime: { hhmmss: '05:53:51', milisecs: 21231233 },
};

export const DriverMockTwoMedium: Driver = {
  _id: 'idTestTwo',
  picture: 'pictureTestTwo',
  age: 35,
  name: 'DriverOneTwo',
  team: 'TeamTestTwo',
  races: new Array(RaceMockThreeMedium),
  position: 2,
  points: 18,
  bestTime: { hhmmss: '06:08:43', milisecs: 22123415 },
};

export const DriverMockThreeSlow: Driver = {
  _id: 'idTestThree',
  picture: 'pictureTestThree',
  age: 35,
  name: 'DriverOneThree',
  team: 'TeamTestTwo',
  races: new Array(RaceMockTwoSlow),
  position: 3,
  points: 15,
  bestTime: { hhmmss: '08:55:23', milisecs: 32123415 },
};

export const DriverRaceDetailsMock: DriverRaceDetails = {
  driver: DriverMockFaster,
  race: RaceMockFast,
};

export const DriverRaceDetailsSlowMock: DriverRaceDetails = {
  driver: DriverMockThreeSlow,
  race: RaceMockTwoSlow,
};

export const Providers = {
  TranslateProviderService: {
    provide: TranslateProviderService,
    useValue: {
      initialize: () => {},
    },
  },
  ActivatedRoute: {
    provide: ActivatedRoute,
    useValue: {
      paramMap: of({ get: () => DriverMockFaster._id }),
      queryParams: of({ secs: 5 }),
    },
  },

  Router: {
    provide: Router,
    useValue: {
      navigate: () => {},
    },
  },
};
