import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { Observable, of } from 'rxjs';

interface Translation {
  [key: string]: string | Translation;
}

export class FakeLoader implements TranslateLoader {
  getTranslation(): Observable<Translation> {
    return of({ key: 'value' });
  }
}

export const TranslateTestingModule = TranslateModule.forRoot({
  loader: { provide: TranslateLoader, useClass: FakeLoader },
});
