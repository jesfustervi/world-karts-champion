export function getMiliSeconds(hms: string) {
  try {
    const [hours, minutes, seconds] = hms.split(':');
    const [secs, millisecs] = seconds.split('.') ?? ['0', '0'];
    return (
      (Number(hours) * 60 * 60 + Number(minutes) * 60 + Number(secs)) * 1000 +
      (millisecs ? Number(millisecs) : 0)
    );
  } catch (err) {
    return 0;
  }
}

// points 25 =>1st, 18 => 2nd, 15 =>3rd, 12 => 4th, 10 => 5th, 8 => 6th, 6 => 7th, 4 =>8th, 2 =>9th and 10 =>.
export function getPoints(position: number) {
  if (position < 5) {
    return position === 1 ? 25 : position === 2 ? 18 : position === 3 ? 15 : 12;
  }
  if (position >= 5 && position < 11) {
    return position === 10 ? 1 : (10 - position) * 2;
  }

  return 0;
}

export function getSlidesNumber(
  maxRowsPerSlide: number,
  totalElements: number
) {
  return (
    Math.floor(totalElements / maxRowsPerSlide) +
    (totalElements % maxRowsPerSlide === 0 ? 0 : 1)
  );
}
export function getTimePerSlide(
  numberOfSlides: number,
  maxTimeMilisecs?: number
) {
  return maxTimeMilisecs ? maxTimeMilisecs / numberOfSlides : 3000;
}

export function getMaxRowsPerSlide(windowHeight: number, heightPerRow: number) {
  return Math.floor(windowHeight / heightPerRow);
}
