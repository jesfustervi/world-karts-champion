import {
  getMaxRowsPerSlide,
  getMiliSeconds,
  getPoints,
  getSlidesNumber,
  getTimePerSlide,
} from './utils';

describe('TranslateProviderService', () => {
  it('driver positions should retrieve correct points', () => {
    expect(getPoints(1)).toBe(25);
    expect(getPoints(2)).toBe(18);
    expect(getPoints(3)).toBe(15);
    expect(getPoints(4)).toBe(12);
    expect(getPoints(5)).toBe(10);
    expect(getPoints(6)).toBe(8);
    expect(getPoints(7)).toBe(6);
    expect(getPoints(8)).toBe(4);
    expect(getPoints(9)).toBe(2);
    expect(getPoints(10)).toBe(1);
    expect(getPoints(11)).toBe(0);
  });

  it('get max slide numbers given a max elements per slide and total elements', () => {
    expect(getSlidesNumber(10, 25)).toBe(3);
    expect(getSlidesNumber(30, 25)).toBe(1);
  });

  it('get time per slide given a max time and number of slides', () => {
    expect(getTimePerSlide(2, 8000)).toBe(4000);
    expect(getTimePerSlide(4, 8000)).toBe(2000);
    expect(getTimePerSlide(4)).toBe(3000);
  });

  it('get max rows per slide given a total height and element height', () => {
    expect(getMaxRowsPerSlide(800, 125)).toBe(6);
    expect(getMaxRowsPerSlide(800, 100)).toBe(8);
  });

  it('getMiliSeconds should convert from string to milisecs', () => {
    expect(getMiliSeconds('12:11:23.23')).toBe(43883023);
    expect(getMiliSeconds('13:22:8')).toBe(48128000);
    expect(getMiliSeconds('errorValue')).toBe(0);
  });
});
