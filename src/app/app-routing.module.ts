import { DriverComponent } from './components/driver/driver.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { AutomaticDashboardComponent } from './components/automatic-dashboard/automatic-dashboard.component';

const routes: Routes = [
  { path: 'driver/:id', component: DriverComponent },
  { path: 'dashboard', component: AutomaticDashboardComponent },
  {
    path: '',
    component: HomeComponent,
  },
  {
    path: '**',
    redirectTo: '/',
    pathMatch: 'full',
  },
];
@NgModule({
  declarations: [],
  exports: [RouterModule],
  imports: [RouterModule.forRoot(routes)],
})
export class AppRoutingModule {}
