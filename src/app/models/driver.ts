import { Race } from './race';

export interface Driver {
  _id: string;
  picture: string;
  age: number;
  name: string;
  team: string;
  races: Race[];
  position?: number;
  points?: number;
  bestTime?: { hhmmss: string; milisecs: number };
}
