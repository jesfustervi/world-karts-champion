import { Driver } from './driver';
import { Race } from './race';
export interface DriverRaceDetails {
  driver: Driver;
  race: Race;
}
