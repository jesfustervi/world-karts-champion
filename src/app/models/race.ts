export interface Race {
  name: string;
  time: string;
  timeMilliSecs?: number;
  position?: number;
  points?: number;
}
