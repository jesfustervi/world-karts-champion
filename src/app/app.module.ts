import { RaceDetailsComponent } from './components/driver/driver-race-details/race-details.component';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { HomeComponent } from './components/home/home.component';
import { DashboardDetailComponent } from './components/dashboard-detail/dashboard-detail.component';
import { DriverComponent } from './components/driver/driver.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatTabsModule } from '@angular/material/tabs';
import { AutomaticDashboardComponent } from './components/automatic-dashboard/automatic-dashboard.component';
import { HeaderComponent } from './components/header/header.component';
import { DriversPerRaceComponent } from './components/drivers-per-race/drivers-per-race.component';
import { DriverPerRaceComponent } from './components/drivers-per-race/driver-per-race/driver-per-race.component';
import { GeneralRankingComponent } from './components/general-ranking/general-ranking.component';
import { GeneralRankingDetailsComponent } from './components/general-ranking/general-ranking-details/general-ranking-details.component';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { Observable } from 'rxjs';

interface Translation {
  [key: string]: string | Translation;
}

export class CustomTranslateLoader implements TranslateLoader {
  constructor(private httpClient: HttpClient) {}

  getTranslation(lang: string): Observable<Translation> {
    lang = lang.substr(0, 2);
    return this.httpClient.get<Translation>(`assets/i18n/${lang}.json`);
  }
}

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    DashboardDetailComponent,
    DriverComponent,
    AutomaticDashboardComponent,
    HeaderComponent,
    DriversPerRaceComponent,
    DriverPerRaceComponent,
    RaceDetailsComponent,
    GeneralRankingComponent,
    GeneralRankingDetailsComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatTabsModule,
    HttpClientModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useClass: CustomTranslateLoader,
        deps: [HttpClient],
      },
    }),
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
