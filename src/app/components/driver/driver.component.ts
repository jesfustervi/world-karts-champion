import { Driver } from './../../models/driver';
import { DriverService } from './../../services/driver-service/driver.service';
import { ChangeDetectorRef, Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-driver',
  templateUrl: './driver.component.html',
  styleUrls: ['./driver.component.scss'],
})
export class DriverComponent implements OnInit {
  currentDriver: Driver;
  @Input() set driver(driver: Driver) {
    this.currentDriver = driver;
  }
  selectedIndex = 0;

  constructor(
    public route: ActivatedRoute,
    public driverService: DriverService,
    public changeDetectorRef: ChangeDetectorRef
  ) {}

  ngOnInit(): void {
    this.route.paramMap.subscribe((params) => {
      const driverId = params.get('id');
      if (driverId) {
        this.currentDriver = this.driverService.getDriver(driverId);
      }
    });
  }
}
