import {
  DriverMockFaster,
  DriverMockThreeSlow,
} from './../../shared/tests/providers';
import { TranslateTestingModule } from './../../shared/tests/modules';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DriverComponent } from './driver.component';
import { Providers } from 'src/app/shared/tests/providers';

describe('DriverComponent', () => {
  let component: DriverComponent;
  let fixture: ComponentFixture<DriverComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [DriverComponent],
      providers: [Providers.ActivatedRoute],
      imports: [TranslateTestingModule],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DriverComponent);
    component = fixture.componentInstance;
    component.currentDriver = DriverMockFaster;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('update driver should update currentDriver', () => {
    component.driver = DriverMockThreeSlow;
    expect(component.currentDriver).toEqual(DriverMockThreeSlow);
  });

  it('ngOnInit getDriver from passed id', () => {
    spyOn(component.driverService, 'getDriver').and.returnValue(
      DriverMockFaster
    );

    component.ngOnInit();
    expect(component.driverService.getDriver).toHaveBeenCalledWith(
      DriverMockFaster._id
    );
    expect(component.currentDriver).toEqual(DriverMockFaster);
  });
});
