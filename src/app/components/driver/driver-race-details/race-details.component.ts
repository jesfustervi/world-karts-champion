import { Race } from './../../../models/race';
import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-race-details',
  templateUrl: './race-details.component.html',
  styleUrls: ['./race-details.component.scss'],
})
export class RaceDetailsComponent {
  @Input() race: Race;
  @Input() index: number;
}
