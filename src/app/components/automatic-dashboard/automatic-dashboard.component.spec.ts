import { DriverRaceDetails } from './../../models/driver-race-details';
import {
  DriverMockThreeSlow,
  DriverMockTwoMedium,
  DriverRaceDetailsMock,
  DriverRaceDetailsSlowMock,
} from './../../shared/tests/providers';
import { DriverMockFaster, Providers } from 'src/app/shared/tests/providers';
import { TranslateTestingModule } from './../../shared/tests/modules';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AutomaticDashboardComponent } from './automatic-dashboard.component';

describe('AutomaticDashboardComponent', () => {
  let component: AutomaticDashboardComponent;
  let fixture: ComponentFixture<AutomaticDashboardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [AutomaticDashboardComponent],
      providers: [Providers.ActivatedRoute],
      imports: [TranslateTestingModule],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AutomaticDashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('getTranslations should retrieve all translations labels', () => {
    component['getTranslations']();
    expect(component.translatedTitleLabels).toEqual(component.titleLabels);
    expect(component.labelName).toEqual(component.titleLabels[0]);
  });

  it('getNextTabIndex should retrieve next index tab', () => {
    expect(component['getNextTabIndex'](5)).toEqual(2);
    expect(component['getNextTabIndex'](3)).toEqual(0);
  });

  it('changeTab should select next tab', () => {
    spyOn(component.changeDetectorRef, 'markForCheck');
    spyOn<any>(component, 'selectNextRaceDrivers');

    component.selectedIndex = 0;
    component.changeTab();
    expect(component.selectedIndex).toEqual(1);
    expect(component['getNextTabIndex'](5)).toEqual(2);
    expect(component.changeDetectorRef.markForCheck).toHaveBeenCalled();
    expect(component['selectNextRaceDrivers']).not.toHaveBeenCalled();
  });

  it('changeTab should select Race tab', () => {
    spyOn(component.changeDetectorRef, 'markForCheck');
    spyOn<any>(component, 'selectNextRaceDrivers');
    component.selectedIndex = 1;
    component.changeTab();
    expect(component.selectedIndex).toEqual(2);
    expect(component['getNextTabIndex'](5)).toEqual(2);
    expect(component.changeDetectorRef.markForCheck).toHaveBeenCalled();
    expect(component['selectNextRaceDrivers']).toHaveBeenCalled();
  });

  it('selectNextDriver should select next driver to show ', () => {
    spyOn(component.changeDetectorRef, 'markForCheck');
    component.currentDriverIndex = 1;
    component.drivers = [
      DriverMockFaster,
      DriverMockThreeSlow,
      DriverMockTwoMedium,
    ];
    component['selectNextDriver']();
    expect(component.currentDriver).toEqual(DriverMockThreeSlow);
    expect(component.currentDriver.position).toEqual(2);
    expect(component.currentDriverIndex).toEqual(2);
  });

  it('selectNextRaceDrivers should select next race to show', () => {
    component.currentNumRace = 0;
    const allRacesSortedByDriversTimes: Map<number, DriverRaceDetails[]> =
      new Map<number, DriverRaceDetails[]>();
    allRacesSortedByDriversTimes.set(0, [
      DriverRaceDetailsSlowMock,
      DriverRaceDetailsMock,
    ]);
    component.allRacesSortedByDriversTimes = allRacesSortedByDriversTimes;
    component['selectNextRaceDrivers']();
    expect(component['labelName']).toEqual(
      component.currentDriversRace[0].race.name
    );
    expect(component.currentNumRace).toEqual(0);
  });

  it('ngOnInit load drivers races and start automatic job', () => {
    spyOn(component.driverService, 'getDrivers');
    spyOn(component.driverService, 'getRacesSortedByTimes');
    spyOn(component, 'changeTab');
    spyOn<any>(component, 'getTranslations');

    component.ngOnInit();
    expect(component['defaultTimePerSlide']).toEqual(5 * 1000);

    expect(component.driverService.getDrivers).toHaveBeenCalled();
    expect(component.driverService.getRacesSortedByTimes).toHaveBeenCalled();
    expect(component.changeTab).toHaveBeenCalled();
    expect(component['getTranslations']).toHaveBeenCalled();
  });
});
