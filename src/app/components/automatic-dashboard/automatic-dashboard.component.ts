import { ActivatedRoute } from '@angular/router';
import { TranslateProviderService } from './../../services/translate-provider-service/translate-provider.service';
import { DriverRaceDetails } from './../../models/driver-race-details';
import { DriverService } from './../../services/driver-service/driver.service';
import { Driver } from './../../models/driver';
import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { startWith } from 'rxjs/operators';
import { interval } from 'rxjs';

enum ChampionshipTab {
  GENERAL = 0,
  DRIVER = 1,
  RACE = 2,
}
@Component({
  selector: 'app-automatic-dashboard',
  templateUrl: './automatic-dashboard.component.html',
  styleUrls: ['./automatic-dashboard.component.scss'],
})
export class AutomaticDashboardComponent implements OnInit {
  defaultTimePerSlide = 10000;
  labelName = '';
  columnDrivers: Driver[][] = [];
  selectedIndex = -1;
  currentDriver: Driver;
  currentNumRace = 0;
  drivers: Driver[] = [];
  currentDriverIndex = 0;
  allRacesSortedByDriversTimes: Map<number, DriverRaceDetails[]> = new Map<
    number,
    DriverRaceDetails[]
  >();

  currentDriversRace: DriverRaceDetails[] = [];
  titleLabels = [
    'Wkc.AutomaticDashBoard.Title.GeneralRanking',
    'Wkc.AutomaticDashBoard.Title.Drivers',
  ];

  translatedTitleLabels = [];

  constructor(
    public readonly driverService: DriverService,
    public readonly changeDetectorRef: ChangeDetectorRef,
    private readonly driversService: DriverService,
    private readonly translateProviderService: TranslateProviderService,
    private readonly route: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.drivers = this.driverService.getDrivers();
    this.allRacesSortedByDriversTimes =
      this.driversService.getRacesSortedByTimes();

    this.route.queryParams.subscribe((params) => {
      this.defaultTimePerSlide = params?.secs
        ? Number(params?.secs) * 1000
        : this.defaultTimePerSlide;
    });

    interval(this.defaultTimePerSlide)
      .pipe(startWith({}))
      .subscribe(() => {
        this.changeTab();
      });

    this.getTranslations();
  }

  private getTranslations() {
    this.translateProviderService
      .translateKeys(this.titleLabels)
      .subscribe((labelsTranslated) => {
        this.translatedTitleLabels = labelsTranslated;
        this.labelName = labelsTranslated[0];
      });
  }

  private getNextTabIndex(selectedIndex: number) {
    return selectedIndex % 3;
  }

  changeTab() {
    const tabIndex = this.getNextTabIndex(this.selectedIndex + 1);

    switch (tabIndex) {
      case ChampionshipTab.DRIVER:
        this.selectNextDriver();
      case ChampionshipTab.GENERAL:
        this.labelName = this.translatedTitleLabels[tabIndex];
        break;
      case ChampionshipTab.RACE:
        this.selectNextRaceDrivers();

        break;
    }
    this.selectedIndex = tabIndex;
    this.changeDetectorRef.markForCheck();
  }

  private selectNextDriver() {
    this.currentDriver = this.drivers[this.currentDriverIndex];
    this.currentDriver.position = this.currentDriverIndex + 1;
    this.currentDriverIndex =
      this.currentDriverIndex === this.drivers.length - 1
        ? 0
        : this.currentDriverIndex + 1;
  }

  private selectNextRaceDrivers() {
    this.currentDriversRace = this.allRacesSortedByDriversTimes.get(
      this.currentNumRace
    );
    this.labelName = this.currentDriversRace[0].race.name;

    this.currentNumRace =
      this.currentNumRace === this.allRacesSortedByDriversTimes.size - 1
        ? 0
        : this.currentNumRace + 1;
  }
}
