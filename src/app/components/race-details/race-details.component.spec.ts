import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RaceMockFast } from 'src/app/shared/tests/providers';

import { RaceDetailsComponent } from './race-details.component';

describe('RaceDetailsComponent', () => {
  let component: RaceDetailsComponent;
  let fixture: ComponentFixture<RaceDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [RaceDetailsComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RaceDetailsComponent);
    component = fixture.componentInstance;
    component.race = RaceMockFast;
    component.index = 2;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
