import { DriverMockFaster } from './../../shared/tests/providers';
import { TranslateModule } from '@ngx-translate/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DashboardDetailComponent } from './dashboard-detail.component';

describe('DashboardDetailComponent', () => {
  let component: DashboardDetailComponent;
  let fixture: ComponentFixture<DashboardDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [DashboardDetailComponent],
      imports: [TranslateModule],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DashboardDetailComponent);
    component = fixture.componentInstance;
    component.driver = DriverMockFaster;
    jasmine.clock().install();
    fixture.detectChanges();
  });

  afterEach(() => {
    jasmine.clock().uninstall();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('onClick should emit event', () => {
    spyOn(component.selectedDriver, 'next');
    component.onClick();
    expect(component.selectedDriver.next).toHaveBeenCalledWith(
      DriverMockFaster
    );
  });

  it('onClick should emit event', () => {
    component.ngOnInit();
    jasmine.clock().tick(500);
    expect(component.hidden).toBeFalsy();
  });
});
