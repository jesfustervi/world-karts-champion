import { Driver } from './../../models/driver';
import {
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output,
} from '@angular/core';

@Component({
  selector: 'app-dashboard-detail',
  templateUrl: './dashboard-detail.component.html',
  styleUrls: ['./dashboard-detail.component.scss'],
})
export class DashboardDetailComponent implements OnInit {
  @Input() driver: Driver;
  @Input() position: number;
  @Output() selectedDriver: EventEmitter<Driver> = new EventEmitter<Driver>();
  hidden = true;

  onClick() {
    this.selectedDriver.next(this.driver);
  }

  ngOnInit(): void {
    setTimeout(() => (this.hidden = false));
  }
}
