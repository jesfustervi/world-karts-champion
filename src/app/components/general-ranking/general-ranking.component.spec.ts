import {
  DriverMockFaster,
  DriverMockThreeSlow,
  DriverMockTwoMedium,
} from './../../shared/tests/providers';
import { TranslateTestingModule } from './../../shared/tests/modules';
import { SimpleChanges } from '@angular/core';
import {
  async,
  ComponentFixture,
  fakeAsync,
  TestBed,
} from '@angular/core/testing';
import { Subscription } from 'rxjs';

import { GeneralRankingComponent } from './general-ranking.component';

describe('GeneralRankingComponent', () => {
  let component: GeneralRankingComponent;
  let fixture: ComponentFixture<GeneralRankingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [GeneralRankingComponent],
      imports: [TranslateTestingModule],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GeneralRankingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('ngOnDestroy should  timer Subject', () => {
    component.timerSubject$ = new Subscription();

    expect(component.timerSubject$.closed).toBeFalsy();
    component.ngOnDestroy();
    expect(component.timerSubject$.closed).toBeTruthy();
  });

  it('ngOnChanges should NOT start the job', () => {
    spyOn<any>(component, 'startJob');
    const changes: SimpleChanges = {
      drivers: {
        currentValue: 1,
        previousValue: 1,
        firstChange: false,
        isFirstChange: () => false,
      },
      maxTimeForDriverRaceComponent: {
        currentValue: 2,
        previousValue: 2,
        firstChange: false,
        isFirstChange: () => false,
      },
    };
    component.ngOnChanges(changes);
    expect(component['startJob']).not.toHaveBeenCalled();
  });

  it('ngOnChanges should start the job', () => {
    spyOn<any>(component, 'startJob');
    const changes: SimpleChanges = {
      drivers: {
        currentValue: 1,
        previousValue: 2,
        firstChange: false,
        isFirstChange: () => false,
      },
      maxTimeForDriverRaceComponent: {
        currentValue: 2,
        previousValue: 3,
        firstChange: false,
        isFirstChange: () => false,
      },
    };
    component.ngOnChanges(changes);
    expect(component['startJob']).toHaveBeenCalled();
  });

  it('getPosition should retrieve the current driver position', () => {
    spyOn<any>(component, 'startJob');
    const changes: SimpleChanges = {
      drivers: {
        currentValue: 1,
        previousValue: 2,
        firstChange: false,
        isFirstChange: () => false,
      },
      maxTimeForDriverRaceComponent: {
        currentValue: 2,
        previousValue: 3,
        firstChange: false,
        isFirstChange: () => false,
      },
    };

    const position = 4;
    const currentIndex = 3;
    component.position = position;
    expect(component.getPosition(currentIndex)).toEqual(
      position + currentIndex
    );
  });

  it('startJob should start the job with 2 slides', fakeAsync(() => {
    // spyOn(Array.prototype, 'slice').and.returnValue([]);
    spyOn<any>(component, 'getDriversToShow').and.returnValue([]);

    component['driversSelected'] = [
      DriverMockFaster,
      DriverMockThreeSlow,
      DriverMockTwoMedium,
      DriverMockFaster,
      DriverMockThreeSlow,
      DriverMockTwoMedium,
      DriverMockFaster,
      DriverMockThreeSlow,
      DriverMockTwoMedium,
      DriverMockFaster,
      DriverMockThreeSlow,
      DriverMockTwoMedium,
    ];
    component['maxTimeForDriverRaceComponent'] = 3000;
    spyOnProperty(window, 'innerHeight').and.returnValue(800);

    component['startJob']();
    jasmine.clock().tick(3000);
    expect(component['getDriversToShow']).toHaveBeenCalledTimes(3);
  }));

  it('getDriversToShow should retrieve drivers from until', () => {
    component['driversSelected'] = [
      DriverMockFaster,
      DriverMockThreeSlow,
      DriverMockTwoMedium,
      DriverMockFaster,
      DriverMockThreeSlow,
      DriverMockTwoMedium,
      DriverMockFaster,
      DriverMockThreeSlow,
      DriverMockTwoMedium,
      DriverMockFaster,
      DriverMockThreeSlow,
      DriverMockTwoMedium,
    ];
    expect(component['getDriversToShow'](0, 3)).toEqual([
      DriverMockFaster,
      DriverMockThreeSlow,
      DriverMockTwoMedium,
    ]);
  });
});
