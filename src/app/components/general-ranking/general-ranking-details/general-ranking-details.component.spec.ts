import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { DriverMockFaster } from 'src/app/shared/tests/providers';

import { GeneralRankingDetailsComponent } from './general-ranking-details.component';

describe('GeneralRankingDetailsComponent', () => {
  let component: GeneralRankingDetailsComponent;
  let fixture: ComponentFixture<GeneralRankingDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [GeneralRankingDetailsComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GeneralRankingDetailsComponent);
    component = fixture.componentInstance;
    component.driver = DriverMockFaster;
    component.index = 1;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
