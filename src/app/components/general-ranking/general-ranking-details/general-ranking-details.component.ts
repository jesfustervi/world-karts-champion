import { Driver } from './../../../models/driver';
import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-general-ranking-details',
  templateUrl: './general-ranking-details.component.html',
  styleUrls: ['./general-ranking-details.component.scss'],
})
export class GeneralRankingDetailsComponent implements OnInit {
  currentDriver: Driver;
  @Input() set driver(driver: Driver) {
    this.currentDriver = driver;
  }
  @Input() index: number;

  constructor() {}

  ngOnInit(): void {}
}
