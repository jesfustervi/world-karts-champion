import { Driver } from './../../models/driver';
import {
  Component,
  Input,
  OnChanges,
  OnDestroy,
  SimpleChanges,
} from '@angular/core';
import { Subscription, timer } from 'rxjs';
import { takeWhile } from 'rxjs/operators';
import {
  getMaxRowsPerSlide,
  getSlidesNumber,
  getTimePerSlide,
} from 'src/app/shared/utils';

@Component({
  selector: 'app-general-ranking',
  templateUrl: './general-ranking.component.html',
  styleUrls: ['./general-ranking.component.scss'],
})
export class GeneralRankingComponent implements OnChanges, OnDestroy {
  private driversSelected: Driver[];
  position = 0;
  @Input() set drivers(drivers: Driver[]) {
    this.driversSelected = drivers;
  }

  @Input() maxTimeForDriverRaceComponent: number;
  currentDrivers: Driver[] = [];

  timerSubject$: Subscription;

  ngOnDestroy(): void {
    if (this.timerSubject$) {
      this.timerSubject$.unsubscribe();
    }
  }

  ngOnChanges(changes: SimpleChanges) {
    if (
      changes.drivers.currentValue !== changes.drivers.previousValue &&
      changes.maxTimeForDriverRaceComponent.currentValue !==
        changes.maxTimeForDriverRaceComponent.previousValue
    ) {
      this.startJob();
    }
  }

  getPosition(currentIndex: number) {
    return this.position + currentIndex;
  }
  private startJob() {
    const minImageSize = 64;
    const headerHeight = 28;
    const tabHeight = 48;

    const maxRowsPerSlide = getMaxRowsPerSlide(
      window.innerHeight - headerHeight - tabHeight,
      minImageSize
    );

    const numberOfSlides = getSlidesNumber(
      maxRowsPerSlide,
      this.driversSelected.length
    );

    const timePerSlide = getTimePerSlide(
      numberOfSlides,
      this.maxTimeForDriverRaceComponent
    );

    let from = 0;
    let until = maxRowsPerSlide;

    this.timerSubject$ = timer(0, timePerSlide)
      .pipe(
        takeWhile((slideNumber) => {
          this.currentDrivers = this.getDriversToShow(from, until);

          until += maxRowsPerSlide;

          from += maxRowsPerSlide;

          if (until > this.driversSelected.length) {
            until = this.driversSelected.length;
          }
          this.position = slideNumber * maxRowsPerSlide;
          return slideNumber < numberOfSlides;
        }, true)
      )
      .subscribe();
  }

  private getDriversToShow(from: number, until: number) {
    return this.driversSelected.slice(from, until);
  }
}
