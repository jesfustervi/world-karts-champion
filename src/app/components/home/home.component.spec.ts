import { DriverMockFaster } from './../../shared/tests/providers';
import { TranslateTestingModule } from './../../shared/tests/modules';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { HomeComponent } from './home.component';
import { Providers } from 'src/app/shared/tests/providers';

describe('HomeComponent', () => {
  let component: HomeComponent;
  let fixture: ComponentFixture<HomeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [HomeComponent],
      providers: [Providers.Router],
      imports: [TranslateTestingModule],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should driverSelected should navigate to the drive id information', () => {
    spyOn(component['router'], 'navigate');
    component.driverSelected(DriverMockFaster);
    expect(component['router'].navigate).toHaveBeenCalledWith([
      '/driver/',
      DriverMockFaster._id,
    ]);
  });

  it('ngOnInit should get all Drivers calling getDrivers Services', () => {
    const driversResult = [DriverMockFaster];
    spyOn(component.driverService, 'getDrivers').and.returnValue(driversResult);
    component.ngOnInit();
    expect(component.driverService.getDrivers).toHaveBeenCalled();
    expect(component.drivers).toEqual(driversResult);
  });
});
