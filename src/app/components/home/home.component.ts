import { Router } from '@angular/router';
import { Driver } from './../../models/driver';
import { DriverService } from './../../services/driver-service/driver.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit {
  drivers: Driver[] = [];

  constructor(public driverService: DriverService, public router: Router) {}
  driverSelected(driver: Driver) {
    this.router.navigate(['/driver/', driver._id]);
  }

  ngOnInit(): void {
    this.drivers = this.driverService.getDrivers();
  }
}
