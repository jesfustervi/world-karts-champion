import {
  DriverRaceDetailsMock,
  DriverRaceDetailsSlowMock,
} from './../../shared/tests/providers';
import { TranslateTestingModule } from './../../shared/tests/modules';
import {
  async,
  ComponentFixture,
  fakeAsync,
  TestBed,
} from '@angular/core/testing';
import { Subscription } from 'rxjs';

import { DriversPerRaceComponent } from './drivers-per-race.component';
import { CUSTOM_ELEMENTS_SCHEMA, SimpleChanges } from '@angular/core';

describe('DriversPerRaceComponent', () => {
  let component: DriversPerRaceComponent;
  let fixture: ComponentFixture<DriversPerRaceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [DriversPerRaceComponent],
      imports: [TranslateTestingModule],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DriversPerRaceComponent);
    component = fixture.componentInstance;
    jasmine.clock().install();
    fixture.detectChanges();
  });

  afterEach(() => {
    jasmine.clock().uninstall();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('ngOnDestroy should  timer Subject', () => {
    component.timerSubject$ = new Subscription();

    expect(component.timerSubject$.closed).toBeFalsy();
    component.ngOnDestroy();
    expect(component.timerSubject$.closed).toBeTruthy();
  });

  it('ngOnChanges should NOT start the job', () => {
    spyOn<any>(component, 'startJob');
    const changes: SimpleChanges = {
      driversRaceDetails: {
        currentValue: 1,
        previousValue: 1,
        firstChange: false,
        isFirstChange: () => false,
      },
      maxTimeForDriverRaceComponent: {
        currentValue: 2,
        previousValue: 2,
        firstChange: false,
        isFirstChange: () => false,
      },
    };
    component.ngOnChanges(changes);
    expect(component['startJob']).not.toHaveBeenCalled();
  });

  it('ngOnChanges should start the job', () => {
    spyOn<any>(component, 'startJob');
    const changes: SimpleChanges = {
      driversRaceDetails: {
        currentValue: 1,
        previousValue: 2,
        firstChange: false,
        isFirstChange: () => false,
      },
      maxTimeForDriverRaceComponent: {
        currentValue: 2,
        previousValue: 3,
        firstChange: false,
        isFirstChange: () => false,
      },
    };
    component.ngOnChanges(changes);
    expect(component['startJob']).toHaveBeenCalled();
  });

  it('startJob should start the job with 2 slides', fakeAsync(() => {
    // spyOn(Array.prototype, 'slice').and.returnValue([]);
    spyOn<any>(component, 'getRacesToShow').and.returnValue([]);

    component['racesDriversDetails'] = [
      DriverRaceDetailsMock,
      DriverRaceDetailsMock,
      DriverRaceDetailsMock,
      DriverRaceDetailsMock,
      DriverRaceDetailsMock,
      DriverRaceDetailsMock,
      DriverRaceDetailsMock,
      DriverRaceDetailsMock,
      DriverRaceDetailsMock,
      DriverRaceDetailsMock,
      DriverRaceDetailsMock,
      DriverRaceDetailsMock,
    ];
    component['maxTimeForDriverRaceComponent'] = 3000;
    spyOnProperty(window, 'innerHeight').and.returnValue(800);

    component['startJob']();
    jasmine.clock().tick(3000);
    expect(component['getRacesToShow']).toHaveBeenCalledTimes(3);
  }));

  it('getRacesToShow should retrieve races from until', () => {
    component['racesDriversDetails'] = [
      DriverRaceDetailsMock,
      DriverRaceDetailsSlowMock,
      DriverRaceDetailsMock,
      DriverRaceDetailsSlowMock,
      DriverRaceDetailsSlowMock,
      DriverRaceDetailsSlowMock,
    ];

    expect(component['getRacesToShow'](1, 4)).toEqual([
      DriverRaceDetailsSlowMock,
      DriverRaceDetailsMock,
      DriverRaceDetailsSlowMock,
    ]);
  });
});
