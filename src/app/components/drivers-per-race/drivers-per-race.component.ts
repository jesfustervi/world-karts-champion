import { takeWhile } from 'rxjs/operators';
import { DriverRaceDetails } from './../../models/driver-race-details';
import {
  Component,
  Input,
  OnChanges,
  OnDestroy,
  SimpleChanges,
} from '@angular/core';
import { Subscription, timer } from 'rxjs';
import {
  getMaxRowsPerSlide,
  getSlidesNumber,
  getTimePerSlide,
} from 'src/app/shared/utils';

@Component({
  selector: 'app-drivers-per-race',
  templateUrl: './drivers-per-race.component.html',
  styleUrls: ['./drivers-per-race.component.scss'],
})
export class DriversPerRaceComponent implements OnChanges, OnDestroy {
  private racesDriversDetails: DriverRaceDetails[];
  @Input() set driversRaceDetails(driversRaceDetails: DriverRaceDetails[]) {
    this.racesDriversDetails = driversRaceDetails;
  }
  @Input() maxTimeForDriverRaceComponent: number;

  currentDriversRaceDetails: DriverRaceDetails[] = [];
  timerSubject$: Subscription;

  ngOnDestroy(): void {
    if (this.timerSubject$) {
      this.timerSubject$.unsubscribe();
    }
  }

  ngOnChanges(changes: SimpleChanges) {
    if (
      changes.driversRaceDetails.currentValue !==
        changes.driversRaceDetails.previousValue &&
      changes.maxTimeForDriverRaceComponent.currentValue !==
        changes.maxTimeForDriverRaceComponent.previousValue
    ) {
      this.startJob();
    }
  }

  private startJob() {
    const minImageSize = 64;
    const headerHeight = 28;
    const tabHeight = 48;

    const maxRowsPerSlide = getMaxRowsPerSlide(
      window.innerHeight - headerHeight - tabHeight,
      minImageSize
    );
    const numberOfSlides = getSlidesNumber(
      maxRowsPerSlide,
      this.racesDriversDetails.length
    );
    const timePerSlide = getTimePerSlide(
      numberOfSlides,
      this.maxTimeForDriverRaceComponent
    );

    let from = 0;
    let until = maxRowsPerSlide;

    this.timerSubject$ = timer(0, timePerSlide)
      .pipe(
        takeWhile((slides) => {
          this.currentDriversRaceDetails = this.getRacesToShow(from, until);

          until += maxRowsPerSlide;
          from += maxRowsPerSlide;
          if (from > this.racesDriversDetails.length) {
            from = this.racesDriversDetails.length;
          }
          return slides < numberOfSlides;
        }, true)
      )
      .subscribe();
  }

  private getRacesToShow(from: number, until: number) {
    return this.racesDriversDetails.slice(from, until);
  }
}
