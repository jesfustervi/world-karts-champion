import { DriverRaceDetails } from './../../../models/driver-race-details';
import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-driver-per-race',
  templateUrl: './driver-per-race.component.html',
  styleUrls: ['./driver-per-race.component.scss'],
})
export class DriverPerRaceComponent {
  @Input() driverRaceDetails: DriverRaceDetails;
  @Input() index: number;

  constructor() {}
}
