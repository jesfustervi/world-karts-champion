import { TranslateTestingModule } from './../../../shared/tests/modules';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { DriverPerRaceComponent } from './driver-per-race.component';
import { DriverRaceDetailsMock } from 'src/app/shared/tests/providers';

describe('DriverPerRaceComponent', () => {
  let component: DriverPerRaceComponent;
  let fixture: ComponentFixture<DriverPerRaceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [DriverPerRaceComponent],
      imports: [TranslateTestingModule],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DriverPerRaceComponent);
    component = fixture.componentInstance;
    component.driverRaceDetails = DriverRaceDetailsMock;
    component.index = 1;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
