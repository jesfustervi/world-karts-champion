import { TranslateTestingModule } from './../../shared/tests/modules';
import { TestBed } from '@angular/core/testing';
import { TranslateService } from '@ngx-translate/core';

import { TranslateProviderService } from './translate-provider.service';
import { of } from 'rxjs';

describe('TranslateProviderService', () => {
  let service: TranslateProviderService;
  let translateService: TranslateService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [TranslateTestingModule],
    });

    service = TestBed.inject(TranslateProviderService);
    translateService = TestBed.inject(TranslateService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('when service initalize should add languages to the service translate and should call setLanguage', () => {
    spyOn<any>(service, 'setLanguage');
    service.initialize();
    expect(service['setLanguage']).toHaveBeenCalled();
    expect(service['translate'].getLangs()).toEqual(['en', 'es']);
  });

  it('Set language should set translate service language to spanish', () => {
    spyOn(translateService, 'getBrowserLang').and.returnValue('es');
    translateService.addLangs(['en', 'es']);
    service['setLanguage']();
    expect(translateService.currentLang).toEqual('es');
  });

  it('should translate the given keys', () => {
    spyOn(translateService, 'get').and.returnValue(of('testTranslation'));
    service.translateKeys(['testKey1', 'testKey2']);
    expect(translateService.get).toHaveBeenCalledTimes(2);
  });
});
