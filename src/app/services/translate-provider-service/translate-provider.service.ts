import { Injectable } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { forkJoin, Observable, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class TranslateProviderService {
  private readonly DEFAULT_LANG: string = 'en';
  public readonly onLangChange$: Subject<string> = new Subject();

  constructor(private readonly translate: TranslateService) {
    translate.setDefaultLang(this.DEFAULT_LANG);
  }

  public initialize() {
    this.translate.addLangs(['en', 'es']);
    this.setLanguage();
  }

  private setLanguage() {
    const browserLang = this.translate.getBrowserLang();
    this.translate.getLangs().includes(browserLang)
      ? this.translate.use(browserLang)
      : this.translate.use(this.DEFAULT_LANG);
  }

  public translateKeys(keys: string[]) {
    const translations = keys.map((key) => {
      return this.translate.get(key) as Observable<string>;
    });

    return forkJoin(translations);
  }
}
