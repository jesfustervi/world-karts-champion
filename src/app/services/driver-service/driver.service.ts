import { DriverRaceDetails } from './../../models/driver-race-details';
import { Driver } from './../../models/driver';
import { Injectable } from '@angular/core';
import { getMiliSeconds, getPoints } from './../../shared/utils';
import drivers_file from './../../../../src/drivers_karts_Front.json';

@Injectable({
  providedIn: 'root',
})
export class DriverService {
  private drivers: Driver[];
  private racesSortedByPilotsTimes: Map<number, DriverRaceDetails[]> = new Map<
    number,
    DriverRaceDetails[]
  >();

  constructor() {
    this.drivers = this.extractDriversSortedByPositions(drivers_file);
  }

  getDrivers() {
    return this.drivers;
  }

  getRacesSortedByTimes() {
    this.racesSortedByPilotsTimes =
      this.racesSortedByPilotsTimes.size !== 0
        ? this.racesSortedByPilotsTimes
        : this.extractChampionRacesSortedByTime(this.drivers);
    return this.racesSortedByPilotsTimes;
  }

  getDriver(driverId: string) {
    const index = this.drivers.findIndex((driver) => driver._id === driverId);
    if (index !== -1) {
      this.drivers[index].position = index + 1;
      return this.drivers[index];
    } else {
      return null;
    }
  }

  private extractDriversSortedByPositions(drivers: Driver[]) {
    this.racesSortedByPilotsTimes =
      this.extractChampionRacesSortedByTime(drivers);

    const driversWithRacePositions = drivers.map((driver) => {
      driver.points = 0;
      driver.bestTime = { milisecs: 999999999, hhmmss: '' };
      for (const entry of this.racesSortedByPilotsTimes.entries()) {
        const numRace = entry[0];
        const driverRaceDetails = entry[1];
        const driverRacePosition = this.getDriverRacePosition(
          driverRaceDetails,
          driver._id
        );

        driver.points += getPoints(driverRacePosition);
        driver.races[numRace].position = driverRacePosition;
        driver.races[numRace].points = getPoints(driverRacePosition);
        driver.bestTime =
          driver.bestTime.milisecs > driver.races[numRace].timeMilliSecs
            ? {
                milisecs: driver.races[numRace].timeMilliSecs,
                hhmmss: driver.races[numRace].time,
              }
            : driver.bestTime;
      }
      return driver;
    });

    return this.getDriversSortedByPoints(driversWithRacePositions);
  }

  private getDriversSortedByPoints(drivers: Driver[]) {
    return drivers?.sort((curr, next) => {
      const res = next.points - curr.points;
      return res === 0 ? curr.bestTime.milisecs - next.bestTime.milisecs : res;
    });
  }

  private getDriverRacePosition(races: DriverRaceDetails[], driverId: string) {
    const driverRacePosition = races.findIndex(
      (race) => race.driver._id === driverId
    );
    return driverRacePosition + 1;
  }

  private extractChampionRacesSortedByTime(drivers: Driver[]) {
    const allRaces: Map<number, DriverRaceDetails[]> = new Map<
      number,
      DriverRaceDetails[]
    >();

    drivers.forEach((driver) => {
      driver.races.forEach((race, index) => {
        race.timeMilliSecs = getMiliSeconds(race.time);
        allRaces.has(index)
          ? allRaces.get(index).push({ driver, race })
          : allRaces.set(index, [{ driver, race }]);
      });
    });
    for (const entry of allRaces.entries()) {
      const championshipRaces = entry[1];

      entry[1] = championshipRaces.sort((prev, next) => {
        return prev.race.timeMilliSecs > next.race.timeMilliSecs
          ? 1
          : prev.race.timeMilliSecs < next.race.timeMilliSecs
          ? -1
          : 0;
      });
    }
    return allRaces;
  }
}
