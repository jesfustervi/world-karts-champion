import { DriverRaceDetails } from './../../models/driver-race-details';
import {
  DriverMockFaster,
  DriverMockTwoMedium,
  DriverRaceDetailsMock,
  DriverMockThreeSlow,
} from './../../shared/tests/providers';
import { TestBed } from '@angular/core/testing';

import { DriverService } from './driver.service';

describe('DriverService', () => {
  let service: DriverService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DriverService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('getDrivers should returl drivers', () => {
    spyOn<any>(service, 'extractDriversSortedByPositions').and.returnValue([
      DriverMockFaster,
    ]);
    service['drivers'] = [DriverMockFaster];
    expect(service['drivers']).toEqual([DriverMockFaster]);
  });

  it('getRacesSortedByTimes should retrieve races sorted stored already in racesSortedByPilotsTimes without calling extractChampionRacesSortedByTime function', () => {
    spyOn<any>(service, 'extractDriversSortedByPositions').and.returnValue([
      DriverMockFaster,
    ]);
    spyOn<any>(service, 'extractChampionRacesSortedByTime');
    expect(service['extractChampionRacesSortedByTime']).not.toHaveBeenCalled();
  });

  it('getRacesSortedByTimes should retrieve races sorted by drivers calling extractChampionRacesSortedByTime function', () => {
    spyOn<any>(service, 'extractDriversSortedByPositions').and.returnValue([
      DriverMockFaster,
    ]);
    service['racesSortedByPilotsTimes'] = new Map<
      number,
      DriverRaceDetails[]
    >();
    const resultMap: Map<number, DriverRaceDetails[]> = new Map<
      number,
      DriverRaceDetails[]
    >();
    resultMap.set(1, [DriverRaceDetailsMock]);
    service['drivers'] = [DriverMockFaster];
    spyOn<any>(service, 'extractChampionRacesSortedByTime').and.returnValue(
      resultMap
    );
    expect(service.getRacesSortedByTimes()).toEqual(resultMap);
    expect(service['extractChampionRacesSortedByTime']).toHaveBeenCalledWith([
      DriverMockFaster,
    ]);
  });

  it('getDriver should retrieve a driver given an driverId ', () => {
    spyOn<any>(service, 'extractDriversSortedByPositions').and.returnValue([
      DriverMockFaster,
    ]);
    service['drivers'] = [DriverMockFaster, DriverMockTwoMedium];
    expect(service.getDriver(DriverMockTwoMedium._id)).toEqual(
      DriverMockTwoMedium
    );
  });

  it('getDriver should retrieve retrieve a null driver given an unknown driverId ', () => {
    spyOn<any>(service, 'extractDriversSortedByPositions').and.returnValue([
      DriverMockFaster,
    ]);

    service['drivers'] = [DriverMockFaster, DriverMockTwoMedium];
    expect(service.getDriver('noneExistentDriverId')).toEqual(null);
  });

  it('getDriversSortedByPoints should retrieve drivers sorted by points ', () => {
    spyOn<any>(service, 'extractDriversSortedByPositions').and.returnValue([
      DriverMockFaster,
    ]);
    const drivers = [
      DriverMockThreeSlow,
      DriverMockFaster,
      DriverMockTwoMedium,
    ];
    const sortedResultDrivers = [
      DriverMockFaster,
      DriverMockTwoMedium,
      DriverMockThreeSlow,
    ];
    expect(service['getDriversSortedByPoints'](drivers)).toEqual(
      sortedResultDrivers
    );
    expect(service['getDriversSortedByPoints'](null)).toEqual(undefined);
  });
});
